function [ classes, classesMap ] = ESC10Classes( )
    % Class map by name
    keySet =   {'001 - Dog bark', '002 - Rain', '003 - Sea waves', '004 - Baby cry', ...
                '005 - Clock tick', '006 - Person sneeze', '007 - Helicopter', ...
                '008 - Chainsaw', '009 - Rooster', '010 - Fire crackling'};
    valueSet = 1:10;
    classesMap = containers.Map(keySet, valueSet);

    % Class map by id
    classes = keySet;
end

