function [ classes, classesMap ] = FINCAClasses( )
    % Class map by name
    keySet =   {'chairs', 'door', 'keyboard', 'cups', 'silence', ...
                'paper', 'laptopkeys', 'pouring', 'speech', 'steps', 'rolling'};
    valueSet = 1:11;
    classesMap = containers.Map(keySet, valueSet);

    % Class map by id
    classes = keySet;
end

