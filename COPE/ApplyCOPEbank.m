function [ V ] = ApplyCOPEbank(config, input, filterbank )
% function [ V ] = ApplyCOPEbank(config, input, filterbank )
% 
% This function computes the response of a set of COPE feature extractors
% in the form of a feature vector.
%
% ApplyCOPEbank takes as input:
%      config     -> A structure with configuration parameters (see SystemConfig.m)
%      input      -> Time-frequency (e.g. Gammatonegram) representation 
%                  of an audio signal to extract the COPE feature from
%      filterbank -> Bank of COPE feature extractors (see
%                  ConfigureCOPE.m)

%
% ApplyCOPEbank returns:
%      V -> A feature vector with the responses of the COPE feature
%           extractors

    % Temporary
    V = zeros(1, numel(filterbank));

    % Preprocess: in the experimetns the filters have all the same sigma0
    % Extend to deal with different values of sigma0 in the filterbank
    [ peaks, location ] = FindLocalMaxima( input, config.FILTERS.localmaxima.nhood, config.FILTERS.localmaxima.threshold );
    %I = peaks;
    I = input;
    
    sigmas = zeros(numel(filterbank), 1);
    for n = 1:numel(filterbank)
        sigmas(n) = filterbank(n).params.sigma0;
    end
    sigmas = unique(sigmas);
    valueSet = cell(numel(sigmas), 1);
    
    % Pre-processing of the input time-frequency representation of the
    % audio signal, for subsequent fast COPE feature extraction
    % NOTE: Here, it ignores the value of "alpha"
    for n = 1:numel(sigmas)
        sigma0 = sigmas(n);
        sigma = sigma0 / 2;
        L = round( 3 * sigma);
        if mod(L, 2) == 0
            L = L + 1;
        end
        w = gaussian2d(L, sigma);

        % Max-blurring for tolerance 
        preproc_input = maxconv2(I, w);
        valueSet{n} = preproc_input;
    end
    
    preprocs = containers.Map(sigmas, valueSet);
    
    % Compute the response of all the filters in the filterbank
    nfilters = numel(filterbank);
    for n = 1:nfilters
        %tic;
        %[score response] = ApplyCOSFIREfilter(config, filterbank(n), input);
        [score, response] = ApplyCOSFIREfilterFAST( filterbank(n), preprocs );
        %toc;
        V(n) = score;
    end
end