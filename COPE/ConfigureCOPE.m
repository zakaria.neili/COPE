function [ model, point ] = ConfigureCOPE(config, input, params, C )
% [ model, point ] = ConfigureCOPE(config, input, params, C )
% 
% Configure a COPE feature extractor from the input sound.
%
% ConfigureCOPE takes as input:
%      config -> A structure with configuration parameters (see SystemConfig.m)
%      input  -> Time-frequency (e.g. Gammatonegram) representation 
%                of a sound of intereste for the configuration of a COPE
%                feature extractors
%      params -> Parameters of the COPE feature (see SystemCOnfig.m -
%                   config.FILTERS.params)
%
% ConfigureCOPE returns:
%      model -> The configured COPE feature extractor
%      point -> The reference point chosen on the input sound
    
    %% TEMPORARY FIX
    if nargin < 4
        C = -1;
    end
    
    %% NOT USED
    %Suppress the responses that are lower than a given threshold t1 from the maximum response
    %input(input < params.COPE.t1*max(input(:))) = 0;
    
    %keypoint(1) = round(size(input, 1)/2);
    %keypoint(2) = point.x;
    
    %% TUPLE CONFIGURATION
    % find application point as the energy peak
    [peak, idx] = max(proto(:));
    [y, x] = ind2sub(size(proto), idx);
    point.x = x;
    point.y = y;
    
    [model.tuples, testMask] = getCOPETuples(config, input, point, params);
    model.params = params;
    model.params.peakvalue = input(point.y, point.x);
    model.class = C;
    model.point = point;
end
   
%% Computation of the tuples in the feature model
function [tuples, testMask, peaks, location] = getCOPETuples(config, input, point, params)
    testMask = zeros(size(input));
    tuples = [];
    
    % Support of the filter (x-dimension)
    supportWindow = params.support;
    
    keypoint(1) = round(size(input, 1) / 2);
    keypoint(2) = point.x;
    
    % For every non zero component (supposed already thresholded) of the
    % input image, extract the information about the position and energy
    startX =  keypoint(2) - round(supportWindow / 2);
    if startX < 1
        startX = 1;
    end
    endX = keypoint(2) + round(supportWindow / 2);
    if endX > size(input,2)
        endX = size(input,2);
    end
    

    peakvalue = input(point.y, point.x);
    
    % Find energy peaks
    if config.FILTERS.localmaxima.method == 0 % CV toolbox
        
        hLocalMax = vision.LocalMaximaFinder;
        hLocalMax.MaximumNumLocalMaxima = 100;
        hLocalMax.NeighborhoodSize = [5 5];
        hLocalMax.Threshold = 0.3*max(input(:,keypoint(2)));
        location = step(hLocalMax, input);
        location = double(location);
        %size of kernel used for application: it is used to find best guess in a
        %square (time-frequency)
        
    elseif config.FILTERS.localmaxima.method == 1 % imregionalmax 
        %[ peaks location ] = FindLocalMaxima( input, config.FILTERS.localmaxima.nhood, config.FILTERS.localmaxima.threshold, peakvalue );
        [ peaks location ] = FindLocalMaxima( input, config.FILTERS.localmaxima.nhood, params.threshold, peakvalue );
    end
    
    % Configure model
    nlocations = size(location, 1);
    for n = 1:nlocations
        %x,y respect to the application point (keypoint)
        x = location(n, 1);
        y = location(n, 2);
        if x >= startX && x <= endX %&& y > round((Nsize+1)/2) && y < size(input,1)-round((Nsize+1)/2) )
            T = zeros(1, 5);
            
            T(1) = x - keypoint(2);
            T(2) = y - keypoint(1);
            T(3) = input(y,x);
            
            distance = sqrt(T(1)^2 + T(2)^2);
            T(4) = distance;
            T(5) = atan2(T(2), T(1));
            %T(6) = gaussian2d(Nsize, params.sigma0 + params.alpha * distance);
            %gaussian2d(Nsize, (supportWindow / 32) + 0.3 * distance);
            
%             tuple.x = x - keypoint(2);
%             tuple.y = y - keypoint(1);
%             tuple.E = input(y,x);
            
            %distance = sqrt(tuple.x^2+tuple.y^2);
            %tuple.distanceFromApplicationPoint=distance;
            %tuple.gaussianMask = gaussian2d(Nsize,(supportWindow/32)+0.3*distance);%this is like std = params.COSFIRE.sigma0+params.COSFIRE.alpha*distances;
            
            tuples = [tuples; T];
            testMask(y, x) = 1;
        end
    end
end

