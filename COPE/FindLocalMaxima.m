function [ peaks, location ] = FindLocalMaxima( input, neighbors, th, peakvalue )
    if nargin < 4
        peakvalue = 0;
%         [peak idx] = max(input(:));
%         [y x] = ind2sub(size(input), idx);
%         point.x = x;
%         point.y = y;
% 
%         peakvalue = input(point.y, point.x);
    end

    % find peaks
    BW = imregionalmax(input, neighbors);
    
    peaks = (input .* BW > peakvalue * th);
    % locations
    ind = find(peaks == 1);
    [Y, X] = ind2sub(size(input), ind);
    location = [X Y];
end

