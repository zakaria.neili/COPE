# MIVIA audio event data set

This is the repository of the MIVIA audio event data set.

The files in the data set are distributed as labeled audio clips of 3 seconds, which contain background sound only or events of interest mixed with background noise at different levels. The events of interest are provided with 8 levels of signal-to-noise ration (SNR), namely -5, 0, 5, 10, 15, 20, 25, 30 dB.

##### Dataset 
The data set (download [here](https://drive.google.com/file/d/1jeUv1iuODyV-8u_RftSYp49cx7431sPJ/view?usp=sharing)) is an easy-to-use version of the original MIVIA audio events data set available at [the MIVIA lab website](http://mivia.unisa.it). The audio clips in the original data set have been segmented with a sliding window of 3 seconds (as in the original paper) and ground truth is provided (available in the directory 'groundtruth'). The audio files are organized in training and test sets.

The naming of the files follows the following structure:
training/{XXXXX_LL}/{XXXXX_LL_NNN}.wav

where XXXXX indicates the audio clip in the original data set (e.g. 00001), LL is the SNR of the events of interest (see table of correspondence in the following), and NNN is the incrementing index of the window that slides on the signal (e.g. 001 is the first the window that start at time 0 seconds, and 005 is the fifth time window that starts at time 4 seconds of the audio clip in the original data set).

| LL  | SNR value  |
|---|---|
| 01  | -5dB  |
| 00  |  0dB |
| 1  | 5dB  |
| 2  | 10dB  |
| 3  | 15dB  |
| 4  | 20dB  |
| 5  | 25dB  |
| 6  | 30dB  |

For each set XXXXX, a corresponding grountruth file is provided in the directory 'groundtruth'.

The class labels are the following:

| Class  | Abbreviation | Label  |
|---|---|---|
| Background | BN | 1  |
| Glass breaking | GB  |  2 |
| Gun shot | GS | 3  |
| Scream | S | 4  |


#### Download
You can download the MIVIA audio event data set at [this url](https://drive.google.com/file/d/1jeUv1iuODyV-8u_RftSYp49cx7431sPJ/view?usp=sharing).

## Evaluation



## References
If you use the data set, please cite the following papers (BibTeX citation format):

Nicola Strisciuglio, Mario Vento, Nicolai Petkov, _Learning representations of sound using trainable COPE feature extractors_, Pattern Recognition, Volume 92, 2019, Pages 25-36, https://doi.org/10.1016/j.patcog.2019.03.016.

	@article{Cope2019,
	title = "Learning representations of sound using trainable COPE feature extractors",
	journal = "Pattern Recognition",
	volume = "92",
	pages = "25 - 36",
	year = "2019",
	doi = "https://doi.org/10.1016/j.patcog.2019.03.016",
	author = "Nicola Strisciuglio and Mario Vento and Nicolai Petkov",
	}

P. Foggia, N. Petkov, A. Saggese, N. Strisciuglio, M. Vento _Reliable detection of audio events in highly noisy environments_ Pattern Recogn. Lett., 65 (2015), pp. 22-28, 10.1016/j.patrec.2015.06.026

	@article{MiviaPRL15,
	title = "Reliable detection of audio events in highly noisy environments ",
	journal = "Pattern Recogn. Lett.",
	volume = "65",
	number = "",
	pages = "22 - 28",
	year = "2015",
	issn = "0167-8655",
	doi = "10.1016/j.patrec.2015.06.026",
	author = "Pasquale Foggia and Nicolai Petkov and Alessia Saggese and Nicola Strisciuglio and Mario Vento",
	}


